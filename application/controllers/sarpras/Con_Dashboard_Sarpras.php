<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_Dashboard_Sarpras extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
//        $this->load->model('bk/Model_Master_Bk','model');
        
//        if ($this->session->userdata('logged')<>1)
//        {
//            redirect(site_url('sarpras/Con_Auth'));
//        }
    }

    public function index()
    {
        $this->load->helper('url');          
        $this->load->view('inti/header_sarpras/header_vmb');
        $this->load->view('sarpras/View_Dashboard_Sarpras');        
        // echo "<h1> TAMPILAN BK </h1>";        
        $this->load->view('inti/Footer');
    }

    // awal untuk mengetes saja
//    public function backupviewcrud()
//    {
//        $this->load->view('isi/backupviewcrud');
//    }
//    // selesai untuk mengetes
//
//    public function ajax_list()
//    {
//        $list = $this->model->get_datatables();
//        $data = array();
//        $no = $_POST['start'];
//        foreach ($list as $model) {
//            $no++;
//            $row = array();
//            $row[] = $model->pointPelanggaran;
//            $row[] = $model->namaPelanggaran;
//            // $row[] = $concrud->gender;
//            // $row[] = $concrud->address;
//            // $row[] = $concrud->dob;
//
//            //add html for action
//            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit_person('."'".$model->idPelanggaran."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
//                  <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_person('."'".$model->idPelanggaran."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
//        
//            $data[] = $row;
//        }
//
//        $output = array(
//                        "draw" => $_POST['draw'],
//                        "recordsTotal" => $this->model->count_all(),
//                        "recordsFiltered" => $this->model->count_filtered(),
//                        "data" => $data,
//                );
//        //output to json format
//        echo json_encode($output);
//    }
//
//    public function ajax_edit($idPelanggaran)
//    {
//        $data = $this->model->get_by_id($idPelanggaran);
//        echo json_encode($data);
//    }
//
//    public function ajax_add()
//    {
//        $data = array(
//                'pointPelanggaran' => $this->input->post('pointPelanggaran'),
//                'namaPelanggaran' => $this->input->post('namaPelanggaran'),
//                // 'gender' => $this->input->post('gender'),
//                // 'address' => $this->input->post('address'),
//                // 'dob' => $this->input->post('dob'),
//            );
//        $insert = $this->model->save($data);
//        echo json_encode(array("status" => TRUE));
//    }
//
//    public function ajax_update()
//    {
//        $data = array(
//                'pointPelanggaran' => $this->input->post('pointPelanggaran'),
//                'namaPelanggaran' => $this->input->post('namaPelanggaran'),
//                // 'gender' => $this->input->post('gender'),
//                    // 'address' => $this->input->post('address'),
//                // 'dob' => $this->input->post('dob'),
//            );
//        $this->model->update(array('idPelanggaran' => $this->input->post('idPelanggaran')), $data);
//        echo json_encode(array("status" => TRUE));
//    }
//
//    public function ajax_delete($idPelanggaran)
//    {
//        $this->model->delete_by_id($idPelanggaran);
//        echo json_encode(array("status" => TRUE));
//    }

}
