<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_Master_Sarpras_Kategori extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sarpras/kategori/Model_Master_Sarpras_Kategori','model');
    }

    public function index()
    {
         $this->load->helper('url');
         $this->load->view('inti/header_sarpras/Header_Vmb');
         $this->load->view('sarpras/kategori/View_Master_Sarpras_Kategori');
         $this->load->view('inti/Footer');
         //echo "<h1> TAMPILAN KESISWAAN </h1>";
    }

    // awal untuk mengetes saja
    public function backupviewcrud()
    {
        $this->load->view('isi/backupviewcrud');
    }
    // selesai untuk mengetes

    public function ajax_list()
    {
        $list = $this->model->get_datatables();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $model) { //untuk menampilkan data yang ada pada database
            $no++;
            $row = array();
            $row[] = $model->jenisKategori;
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit_person('."'".$model->idKategori."'".')"><i class="glyphicon glyphicon-pencil"></i> </a>
                  <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_person('."'".$model->idKategori."'".')"><i class="glyphicon glyphicon-trash"></i></a>';

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_edit($idKategori)
    {
        $data = $this->model->get_by_id($idKategori);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
                'jenisKategori' => $this->input->post('jenisKategori'),
            );
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
                'jenisKategori' => $this->input->post('jenisKategori'),
            );
        $this->model->update(array('idKategori' => $this->input->post('idKategori')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($idKategori)
    {
        $this->model->delete_by_id($idKategori);
        echo json_encode(array("status" => TRUE));
    }

}
