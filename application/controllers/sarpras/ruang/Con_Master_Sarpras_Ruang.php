<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_Master_Sarpras_Ruang extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sarpras/ruang/Model_Master_Sarpras_Ruang','model');
    }

    public function index()
    {
         $this->load->helper('url');
         $this->load->view('inti/header_sarpras/Header_Vmb');
         $this->load->view('sarpras/ruang/View_Master_Sarpras_Ruang');
         //echo "<h1> TAMPILAN KESISWAAN </h1>";
         $this->load->view('inti/Footer');
    }

    // awal untuk mengetes saja
//    public function backupviewcrud()
//    {
//        $this->load->view('isi/backupviewcrud');
//    }
    // selesai untuk mengetes

    public function ajax_list()
    {
        $list = $this->model->get_datatables();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $model) { //untuk menampilkan data yang ada pada database
            $no++;
            $row = array();
            $row[] = $model->jenis_r;
            $row[] = $model->nama_r;
            $row[] = $model->kelas_r;
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit_person('."'".$model->id_r."'".')"><i class="glyphicon glyphicon-pencil"></i> </a>
                  <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_person('."'".$model->id_r."'".')"><i class="glyphicon glyphicon-trash"></i></a>';

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_edit($id_ruang)
    {
        $data = $this->model->get_by_id($id_ruang);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
                'jenis_r' => $this->input->post('jenis_r'),
                'nama_r' => $this->input->post('nama_r'),
                'kelas_r' => $this->input->post('kelas_r')
            );
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
                'jenis_r' => $this->input->post('jenis_r'),
                'nama_r' => $this->input->post('nama_r'),
                'kelas_r' => $this->input->post('kelas_r')
            );
        $this->model->update(array('idRuang' => $this->input->post('idRuang')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id_r)
    {
        $this->model->delete_by_id($id_r);
        echo json_encode(array("status" => TRUE));
    }

}
