<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_Master_Sarpras_Barang extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sarpras/barang/Model_Master_Sarpras_Barang','model');
    }

    public function index()
    {
         $this->load->helper('url');
         $this->load->view('inti/header_sarpras/Header_Vmb');
         $this->load->view('sarpras/barang/View_Master_Sarpras_Barang');
         //echo "<h1> TAMPILAN KESISWAAN </h1>";
         $this->load->view('inti/Footer');
    }

    // awal untuk mengetes saja
//    public function backupviewcrud()
//    {
//        $this->load->view('isi/backupviewcrud');
//    }
    // selesai untuk mengetes

    public function ajax_list()
    {
        $list = $this->model->get_datatables();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $model) { //untuk menampilkan data yang ada pada database
            $no++;
            $row = array();
            $row[] = $model->nama_i;
            $row[] = $model->pengadaan_i;
            $row[] = $model->jumlah_i;
            $row[] = $model->kondisi_i;
            $row[] = $model->kategori_i;
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit_person('."'".$model->id_i."'".')"><i class="glyphicon glyphicon-pencil"></i> </a>
                  <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_person('."'".$model->id_i."'".')"><i class="glyphicon glyphicon-trash"></i></a>';

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_edit($id_bar)
    {
        $data = $this->model->get_by_id($id_bar);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
                'nama_i' => $this->input->post('nama_i'),
                'pengadaan_i' => $this->input->post('pengadaan_i'),
                'jumlah_i' => $this->input->post('jumlah_i'),
                'kondisi_i' => $this->input->post('kondisi_i'),
                'kategori_i' => $this->input->post('kategori_i')
            );
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
                'nama_i' => $this->input->post('nama_i'),
                'pengadaan_i' => $this->input->post('pengadaan_i'),
                'jumlah_i' => $this->input->post('jumlah_i'),
                'kondisi_i' => $this->input->post('kondisi_i'),
                'kategori_i' => $this->input->post('kategori_i')
            );
        $this->model->update(array('id_i' => $this->input->post('id_i')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id_i)
    {
        $this->model->delete_by_id($id_i);
        echo json_encode(array("status" => TRUE));
    }

}
