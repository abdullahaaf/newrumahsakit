<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_Master_Sarpras_Kondisi extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sarpras/kondisi/Model_Master_Sarpras_Kondisi','model');
    }

    public function index()
    {
         $this->load->helper('url');
         $this->load->view('inti/header_sarpras/Header_Vmb');
         $this->load->view('sarpras/kondisi/View_Master_Sarpras_Kondisi');
         $this->load->view('inti/Footer');
         //echo "<h1> TAMPILAN KESISWAAN </h1>";
    }

    // awal untuk mengetes saja
    public function backupviewcrud()
    {
        $this->load->view('isi/backupviewcrud');
    }
    // selesai untuk mengetes

    public function ajax_list()
    {
        $list = $this->model->get_datatables();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $model) { //untuk menampilkan data yang ada pada database
            $no++;
            $row = array();
            $row[] = $model->jenisKondisi;
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit_person('."'".$model->idKondisi."'".')"><i class="glyphicon glyphicon-pencil"></i> </a>
                  <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_person('."'".$model->idKondisi."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_edit($idKondisi)
    {
        $data = $this->model->get_by_id($idKondisi);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
                'jenisKondisi'=> $this->input->post('jenisKondisi'),
            );
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
                'jenisKondisi' => $this->input->post('jenisKondisi'),
            );
        $this->model->update(array('idKondisi' => $this->input->post('idKondisi')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($idKondisi)
    {
        $this->model->delete_by_id($idKondisi);
        echo json_encode(array("status" => TRUE));
    }

}
