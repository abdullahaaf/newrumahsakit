<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// session_start();//untuk session
class Con_Master_Sarpras_Kamar_Settingkamar extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sarpras/kamar/Model_Master_Sarpras_Kamar_Settingkamar','model');
    }

    public function index()
    {
        $this->load->helper('url');
        $this->load->view('inti/header_sarpras/Header_Vmb');
        $data['kamar'] = $this->model->tampil_data_kamar();
        $data['ruangan'] = $this->model->tampil_data_ruang();
        $data['kelas'] = $this->model->tampil_data_kelas();
        $data['gedung'] = $this->model->tampil_data_gedung();
        $this->load->view('sarpras/kamar/View_Master_Sarpras_Kamar_Settingkamar',$data);
        $this->load->view('inti/Footer');
    }

    public function ajax_list()
    {
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $urut = 1;
        foreach ($list as $model) {
            // $no++;
            $row = array();
            $row[] = $model->namaKamar;
            $row[] = $model->namaRuangan;
            $row[] = $model->namaKelas;
            $row[] = $model->namaGedung;
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit_person('."'".$model->idSetKamar."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_person('."'".$model->idSetKamar."'".')"><i class="glyphicon glyphicon-trash"></i></a>';

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_edit($idSetKamar)
    {
        $data = $this->model->get_by_id($idSetKamar);
        echo json_encode($data);
    }

    public function ajax_add()
    {

        $data = array(
                'idKamar' => $this->input->post('idKamar'),
                'idRuangan' => $this->input->post('idRuangan'),
                'idKelas' => $this->input->post('idKelas'),
                'idGedung' => $this->input->post('idGedung'),
            );
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {

        $data = array(
          'idKamar' => $this->input->post('idKamar'),
          'idRuangan' => $this->input->post('idRuangan'),
          'idKelas' => $this->input->post('idKelas'),
          'idGedung' => $this->input->post('idGedung'),
            );
        $this->model->update(array('idSetKamar' => $this->input->post('idSetKamar')), $data);
        echo json_encode(array("status" => TRUE));
    }


    public function ajax_delete($idSetKamar)
    {
        $this->model->delete_by_id($idSetKamar);
        echo json_encode(array("status" => TRUE));
    }

}
