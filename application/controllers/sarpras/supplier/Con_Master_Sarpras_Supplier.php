<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_Master_Sarpras_Supplier extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sarpras/supplier/Model_Master_Sarpras_Supplier','model');
    }

    public function index()
    {
         $this->load->helper('url');
         $this->load->view('inti/header_sarpras/Header_Vmb');
         $this->load->view('sarpras/supplier/View_Master_Sarpras_Supplier');
         //echo "<h1> TAMPILAN KESISWAAN </h1>";
         $this->load->view('inti/Footer');
    }

    // awal untuk mengetes saja
    public function backupviewcrud()
    {
        $this->load->view('isi/backupviewcrud');
    }
    // selesai untuk mengetes

    public function ajax_list()
    {
        $list = $this->model->get_datatables();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $model) { //untuk menampilkan data yang ada pada database
            $no++;
            $row = array();
            $row[] = $model->namaSupplier;
           $row[] = $model->alamat;
//            $row[] = $model->jumlah;
//            $row[] = $model->keadaan;

            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit_person('."'".$model->idSupplier."'".')"><i class="glyphicon glyphicon-pencil"></i> </a>
                  <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_person('."'".$model->idSupplier."'".')"><i class="glyphicon glyphicon-trash"></i></a>';

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_edit($idSupplier)
    {
        $data = $this->model->get_by_id($idSupplier);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
                'namaSupplier' => $this->input->post('namaSupplier'),
           'alamat' => $this->input->post('alamat'),
//            'idSupplier' => $this->input->post('idSupplier'),
//            'jumlah' => $this->input->post('jumlah'),
//            'keadaan' => $this->input->post('keadaan'),
            );
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
                'namaSupplier' => $this->input->post('namaSupplier'),
           'alamat' => $this->input->post('alamat'),
//            'jumlah' => $this->input->post('jumlah'),
//            'keadaan' => $this->input->post('keadaan'),
            );
        $this->model->update(array('idSupplier' => $this->input->post('idSupplier')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($idSupplier)
    {
        $this->model->delete_by_id($idSupplier);
        echo json_encode(array("status" => TRUE));
    }

}
