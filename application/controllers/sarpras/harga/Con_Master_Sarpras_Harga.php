<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_Master_Sarpras_Harga extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sarpras/harga/Model_Master_Sarpras_Harga','model');
    }

    public function index()
    {
         $this->load->helper('url');
         $this->load->view('inti/header_sarpras/Header_Vmb');
         $data['qinventaris'] = $this->model->get_nama_inventaris();
         $this->load->view('sarpras/harga/View_Master_Sarpras_Harga');
         $this->load->view('inti/Footer');
         //echo "<h1> TAMPILAN KESISWAAN </h1>";
    }

    // awal untuk mengetes saja
    public function backupviewcrud()
    {
        $this->load->view('isi/backupviewcrud');
    }
    // selesai untuk mengetes

    public function ajax_list()
    {
        $list = $this->model->get_datatables();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $model) { //untuk menampilkan data yang ada pada database
            $no++;
            $row = array();
            $row[] = $model->namaInventaris;
            $row[] = $model->hargaInventaris;
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit_person('."'".$model->idNilai."'".')"><i class="glyphicon glyphicon-pencil"></i> </a>
                  <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_person('."'".$model->idNilai."'".')"><i class="glyphicon glyphicon-trash"></i></a>';

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_edit($idNilai)
    {
        $data = $this->model->get_by_id($idNilai);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
                'idInventaris' => $this->input->post('idInventaris'),
                'hargaInventaris' => $this->input->post('hargaInventaris'),
            );
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
                'idInventaris' => $this->input->post('namaInventaris'),
                'hargaInventaris' => $this->input->post('hargaInventaris'),
            );
        $this->model->update(array('idNilai' => $this->input->post('idNilai')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($idNilai)
    {
        $this->model->delete_by_id($idNilai);
        echo json_encode(array("status" => TRUE));
    }

}
