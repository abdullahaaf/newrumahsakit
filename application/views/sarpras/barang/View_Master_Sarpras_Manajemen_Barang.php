<div class="box box-primary">
    <div class="box-header with-border">
    <h1>Selamat Datang di Sistem Manajemen Aset Rumah Sakit</h1>

    <h3>Manajemen Barang</h3>
    <br />
    <button class="btn btn-success" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i> Tambah Data</button>
    <br />
    <br />
    <table id="table" class="table table-bordered table-hover" cellspacing="0" width="100%">
      <thead>
        <tr class="success">
          <th>Nama Ruangan</th>
          <th>Nama Barang</th>
          <th>Jumlah Barang</th>
          <th style="width:125px;">Action</th>
        </tr>
      </thead>
      <tbody>
      </tbody>

      <tfoot>
        <tr class="success">
          <th>Nama Ruangan</th>
          <th>Nama Barang</th>
          <th>Jumlah Barang</th>
          <th>Action</th>
        </tr>taqtik.com
      </tfoot>
    </table>
    </div>
  </div>

  <script type="text/javascript">

    var save_method; //for save method string
    var table;
    $(document).ready(function() {
      table = $('#table').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('sarpras/barang/Con_Master_Sarpras_Manajemen_Barang/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        {
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });

    function add_person()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Tambah Data'); // Set Title to Bootstrap modal title
    }

    function edit_person(id_mb)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('sarpras/barang/Con_Master_Sarpras_Manajemen_Barang/ajax_edit/')?>/" + id_mb,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id_mb"]').val(data.id_mb);
            $('[name="id_r"]').val(data.id_r);
            $('[name="id_i"]').val(data.id_i);

            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Supplier'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax
    }

    function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('sarpras/barang/Con_Master_Sarpras_Manajemen_Barang/ajax_add')?>";
      }
      else
      {
        url = "<?php echo site_url('sarpras/barang/Con_Master_Sarpras_Manajemen_Barang/ajax_update')?>";
      }

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_person(id_mb)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "<?php echo site_url('sarpras/barang/Con_Master_Sarpras_Barang/ajax_delete')?>/"+id_mb,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });

      }
    }

  </script>

<!-- ====================================== Untuk Input Mask ======================================= -->
    <!-- Select2 -->
    <script src="<?=base_url('assets/plugins/select2/select2.full.min.js')?>"></script>
    <!-- InputMask -->
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.js')?>"></script>
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')?>"></script>
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js')?>"></script>
<!-- ================================================================================================ -->

   <!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
        //Money Euro
        $("[data-mask]").inputmask();
      });
    </script>

  <!-- Bootstrap modal -->
  <div class="modal fade modal-primary" id="modal_form" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Form Manajemen Barang</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="id_mb"/>
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Ruangan</label>
              <div class="col-md-9">
                <div id="comboxruangan">
                  <select class="form-control" name="id_r" id="id_r">
                    <option></option>
                    <?php foreach($ruang as $r){?>
                    <option value="<?=$r->id_r?>"><?=$r->nama_r." ".$r->kelas_r?></option>
                    <?php }?>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Nama Barang</label>
                <div class="col-md-9">
                  <div id="comboxbarang">
                    <select class="form-control" name="id_i" id="id_i">
                      <?php foreach($barang as $b){?>
                      <option value="<?=$b->id_i?>"><?=$b->nama_i?></option>
                      <?php }?>
                    </select>
                  </div>
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Jumlah Barang</label>
                <div class="col-md-9">
                    <input type="text" name="jumlahBarang" placeholder="Jumlah Barang" class="form-control">
                </div>
            </div>
          </div>
        </form>
      </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
