
<!-- ===================== Awal isi tab 1 =====================  -->
	<div class="box box-primary" style="overflow-x:scroll;">
		<div class="box-header with-border">
		<center><h1>Setting Kamar</h1></center>
											<br />
											<button class="btn btn-success" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i>Setting Kamar</button>
											<br />
											<br />
											<table id="table" class="table table-bordered table-hover" cellspacing="0" width="100%">
												<thead>
													<tr class="success">
														<th>Kamar</th>
														<th>Ruang</th>
														<th>Kelas</th>
														<th>Gedung</th>
                            <th style="width:50px;">Action</th>
													</tr>
												</thead>
												<tbody>
												</tbody>

												<tfoot>
													<tr class="success">
                            <th>Kamar</th>
														<th>Ruang</th>
														<th>Kelas</th>
														<th>Gedung</th>
                            <th>Action</th>
													</tr>
												</tfoot>
											</table>
											</div>
										</div>

<!-- ===================== Akhir Tab Navigation =====================  -->

	<script type="text/javascript">

		var save_method; //for save method string
		var table;
		$(document).ready(function() {
			table = $('#table').DataTable({

				"processing": true, //Feature control the processing indicator.
				"serverSide": true, //Feature control DataTables' server-side processing mode.

				// Load data for the table's content from an Ajax source
				"ajax": {
						"url": "<?php echo site_url('sarpras/kamar/Con_Master_Sarpras_Kamar_Settingkamar/ajax_list')?>",
						"type": "POST"
				},

				//Set column definition initialisation properties.
				"columnDefs": [
				{
					"targets": [ -1 ], //last column
					"orderable": false, //set not orderable
				},
				],

			});
		});

		function add_person()
		{
			save_method = 'add';
			$('#form')[0].reset();
			$('#modal_form').modal('show'); // show bootstrap modal
			$('.modal-title').text('Atur Kelas'); // Set Title to Bootstrap modal title
		}


		function edit_person(idSetKamar)
		{
			save_method = 'update';
			$('#form')[0].reset(); // reset form on modals

			//Ajax Load data from ajax
			$.ajax({
				url : "<?php echo site_url('sarpras/kamar/Con_Master_Sarpras_Kamar_Settingkamar/ajax_edit/')?>/" + idSetKamar,
				type: "GET",
				dataType: "JSON",
				success: function(data)
				{
						$('[name="idSetKamar"]').val(data.idSetKamar);
            $('[name="idKamar"]').val(data.idKamar);
						$('[name="idRuangan"]').val(data.idRuangan);
						$('[name="idKelas"]').val(data.idKelas);
            $('[name="idGedung"]').val(data.idGedung);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
						$('.modal-title').text('Atur Kelas'); // Set title to Bootstrap modal title

						tampildataguru($('[name="nip"]').val());

				},
				error: function (jqXHR, textStatus, errorThrown)
				{
						alert('Error get data from ajax');
				}
		});
		}

		function reload_table()
		{
			table.ajax.reload(null,false); //reload datatable ajax
		}

		function save()
		{
			var url;
			if(save_method == 'add')
			{
				url = "<?php echo site_url('sarpras/kamar/Con_Master_Sarpras_Kamar_Settingkamar/ajax_add')?>";
			}
			else
			{
				url = "<?php echo site_url('sarpras/kamar/Con_Master_Sarpras_Kamar_Settingkamar/ajax_update')?>";
			}

			 // ajax adding data to database
					$.ajax({
						url : url,
						type: "POST",
						data: $('#form').serialize(),
						dataType: "JSON",
						success: function(data)
						{
							 //if success close modal and reload ajax table
							 $('#modal_form').modal('hide');
							 reload_table();
						},
						error: function (jqXHR, textStatus, errorThrown)
						{
								alert('Error adding / update data');
						}
				});
		}

		function delete_person(idSetKamar)
		{
			if(confirm('Are you sure delete this data?'))
			{
				// ajax delete data to database

					$.ajax({
						url : "<?php echo site_url('sarpras/kamar/Con_Master_Sarpras_Kamar_Settingkamar/ajax_delete')?>/"+idSetKamar,
						type: "POST",
						dataType: "JSON",
						success: function(data)
						{
							//if success reload ajax table// <========== data buku

							$('#modal_form').modal('hide');
							reload_table();
						},
						error: function (jqXHR, textStatus, errorThrown)
						{
								alert('Error adding / update data');
						}
					})
			}
		}

	</script>

<!-- ====================================== Untuk Input Mask ======================================= -->
		<!-- Select2 -->
		<script src="<?=base_url('assets/plugins/select2/select2.full.min.js')?>"></script>
		<!-- InputMask -->
		<script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.js')?>"></script>
		<script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')?>"></script>
		<script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js')?>"></script>
<!-- ================================================================================================ -->

	 <!-- Page script -->
		<script>
			$(function () {
				//Initialize Select2 Elements
				$(".select2").select2();
				//Datemask dd/mm/yyyy
				$("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
				//Money Euro
				$("[data-mask]").inputmask();
			});
		</script>

	<!-- Bootstrap modal -->
	<div class="modal fade modal-primary" id="modal_form" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Setting Kamar</h3>
			</div>
			<div class="modal-body form">
				<form action="#" id="form" class="form-horizontal">
				<input type="hidden" value="" name="idSetKamar"/>
					<div class="form-body">
						<!-- ================ batas awal ajax tampil data ================ -->

					<!-- ================ batas bawah ajax tampil data ================ -->

					<!-- ================ batas atas combox jurusan ================ -->

                        <div class="form-group">
              <label class="control-label col-md-3">Kamar</label>
              <div class="col-md-9">
                  <div id="comboxkamar"> <!-- sebagai indentitas combo box -->
                  <select name="idKamar" id="idKamar" class="form-control">
                   <option></option>
                   <?php foreach($kamar as $kamar){?>
                   <option value="<?=$kamar->idKamar?>"><?=$kamar->namaKamar?></option>
                   <?php }?>
                  </select>
                  </div>
                            </div>
                        </div>
						<script>
							$(document).ready(function () {
                  $('#comboxkamar').change(function () {
                  var namaKamar = $(this).val();
                  console.log(namaKamar);  //menampilan pada log browser apa yang dibawa pada saat dipilih pada combo box
                  $.ajax({
                    url: "<?=base_url()?>sarpras/kamar/Con_Master_Sarpras_Kamar_Settingkamar/",       //memanggil function controller dari url
                    async: false,
                    type: "POST",     //jenis method yang dibawa ke function
                    data: "kamar="+namaKamar,   //data yang akan dibawa di url
                    dataType: "html",
                    success: function(data) {
                        alert ("success");   //hasil ditampilkan pada combobox id=kota
                    }
                  })
                  });
                  });
						</script>

                        <div class="form-group">
              <label class="control-label col-md-3">Ruang</label>
              <div class="col-md-9">
                  <div id="comboxruang"> <!-- sebagai indentitas combo box -->
                  <select name="idRuangan" id="idRuangan" class="form-control">
                   <option></option>
                   <?php foreach($ruangan as $ruangan){?>
                   <option value="<?=$ruangan->idRuangan?>"><?=$ruangan->namaRuangan?></option>
                   <?php }?>
                  </select>
                  </div>
                            </div>
                        </div>
						<script>
							$(document).ready(function () {
                  $('#comboxruang').change(function () {
                  var namaRuangan = $(this).val();
                  console.log(namaRuangan);  //menampilan pada log browser apa yang dibawa pada saat dipilih pada combo box
                  $.ajax({
                    url: "<?=base_url()?>sarpras/kamar/Con_Master_Sarpras_Kamar_Settingkamar/",       //memanggil function controller dari url
                    async: false,
                    type: "POST",     //jenis method yang dibawa ke function
                    data: "ruangan="+namaRuangan,   //data yang akan dibawa di url
                    dataType: "html",
                    success: function(data) {
                        alert ("success");   //hasil ditampilkan pada combobox id=kota
                    }
                  })
                  });
                  });
						</script>

                        <div class="form-group">
              <label class="control-label col-md-3">Kelas</label>
              <div class="col-md-9">
                  <div id="comboxkelas"> <!-- sebagai indentitas combo box -->
                  <select name="idKelas" id="idKelas" class="form-control">
                   <option></option>
                   <?php foreach($kelas as $kelas){?>
                   <option value="<?=$kelas->idKelas?>"><?=$kelas->namaKelas?></option>
                   <?php }?>
                  </select>
                  </div>
                            </div>
                        </div>
						<script>
							$(document).ready(function () {
                  $('#comboxkelas').change(function () {
                  var namaKelas = $(this).val();
                  console.log(namaKelas);  //menampilan pada log browser apa yang dibawa pada saat dipilih pada combo box
                  $.ajax({
                    url: "<?=base_url()?>sarpras/kamar/Con_Master_Sarpras_Kamar_Settingkamar/",       //memanggil function controller dari url
                    async: false,
                    type: "POST",     //jenis method yang dibawa ke function
                    data: "kelas="+namaKelas,   //data yang akan dibawa di url
                    dataType: "html",
                    success: function(data) {
                        alert ("success");   //hasil ditampilkan pada combobox id=kota
                    }
                  })
                  });
                  });
						</script>

                        <div class="form-group">
              <label class="control-label col-md-3">Gedung</label>
              <div class="col-md-9">
                  <div id="comboxgedung"> <!-- sebagai indentitas combo box -->
                  <select name="idGedung" id="idGedung" class="form-control">
                   <option></option>
                   <?php foreach($gedung as $gedung){?>
                   <option value="<?=$gedung->idGedung?>"><?=$gedung->namaGedung?></option>
                   <?php }?>
                  </select>
                  </div>
                            </div>
                        </div>
						<script>
							$(document).ready(function () {
                  $('#comboxgedung').change(function () {
                  var namaGedung = $(this).val();
                  console.log(namaGedung);  //menampilan pada log browser apa yang dibawa pada saat dipilih pada combo box
                  $.ajax({
                    url: "<?=base_url()?>sarpras/kamar/Con_Master_Sarpras_Kamar_Settingkamar/",       //memanggil function controller dari url
                    async: false,
                    type: "POST",     //jenis method yang dibawa ke function
                    data: "gedung="+namaGedung,   //data yang akan dibawa di url
                    dataType: "html",
                    success: function(data) {
                        alert ("success");   //hasil ditampilkan pada combobox id=kota
                    }
                  })
                  });
                  });
						</script>
					<!-- ================ batas bawah ajax tampil data ================ -->

								</div>

					<!-- ================ batas bawah ajax tampil data ================ -->

					</div>
				</form>
					</div>
					<div class="modal-footer">
						<button type="button" id="btnSave" onclick="save()" class="btn btn-success">Save</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	<!-- End Bootstrap modal -->
