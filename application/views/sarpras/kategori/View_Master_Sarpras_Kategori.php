<div class="box box-primary">
    <div class="box-header with-border">
    <h1>Selamat Datang di Sistem Inventaris Rumah Sakit</h1>

    <h3>Data Kategori Inventaris </h3>
    <br />
    <button class="btn btn-success" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i> Tambah Data</button>
    <br />
    <br />
    <table id="table" class="table table-bordered table-hover" cellspacing="0" width="100%">
      <thead>
        <tr class="success">
          <th>Kategori Inventaris</th>
<!--
          <th>Jenis Inventaris</th>
          <th>Jumlah</th>
          <th>Keadaan</th>
-->
          <!-- <th>Address</th>
          <th>Date of Birth</th> -->
          <th style="width:125px;">Action</th>
        </tr>
      </thead>
      <tbody>
      </tbody>

      <tfoot>
        <tr class="success">
          <th>Kategori Inventaris</th>
<!--
          <th>Jenis Inventaris</th>
          <th>Jumlah</th>
          <th>Keadaan</th>
-->
         <!--  <th>Address</th>
          <th>Date of Birth</th> -->
          <th>Action</th>
        </tr>
      </tfoot>
    </table>
    </div>
  </div>
<!-- ============ batas tampil ============ -->

  <script type="text/javascript">

    var save_method; //for save method string
    var table;
    $(document).ready(function() {
      table = $('#table').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('sarpras/kategori/Con_Master_Sarpras_Kategori/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        {
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });

    function add_person()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Tambah Data'); // Set Title to Bootstrap modal title
    }

    function edit_person(idKategori)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('sarpras/kategori/Con_Master_Sarpras_Kategori/ajax_edit/')?>/" + idKategori,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="idKategori"]').val(data.idKategori);
            $('[name="namaKategori"]').val(data.namaKategori);
//            $('[name="idKategori"]').val(data.idKategori);
//            $('[name="jumlah"]').val(data.jumlah);
//            $('[name="keadaan"]').val(data.keadaan);
            // $('[name="address"]').val(data.address);
            // $('[name="dob"]').val(data.dob);

            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Inventaris'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax
    }

    function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('sarpras/kategori/Con_Master_Sarpras_Kategori/ajax_add')?>";
      }
      else
      {
        url = "<?php echo site_url('sarpras/kategori/Con_Master_Sarpras_Kategori/ajax_update')?>";
      }

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_person(idKategori)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "<?php echo site_url('sarpras/kategori/Con_Master_Sarpras_Kategori/ajax_delete')?>/"+idKategori,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });

      }
    }

  </script>

<!-- ====================================== Untuk Input Mask ======================================= -->
    <!-- Select2 -->
    <script src="<?=base_url('assets/plugins/select2/select2.full.min.js')?>"></script>
    <!-- InputMask -->
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.js')?>"></script>
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')?>"></script>
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js')?>"></script>
<!-- ================================================================================================ -->

   <!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
        //Money Euro
        $("[data-mask]").inputmask();
      });
    </script>

  <!-- Bootstrap modal -->
  <div class="modal fade modal-primary" id="modal_form" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Form Inventaris</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="idKategori"/>
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Kategori Inventaris</label>
              <div class="col-md-9">
                <input name="jenisKategori" placeholder="Kategori Inventaris" class="form-control" type="text">
              </div>
            </div>
            <!-- <div class="form-group">
              <label class="control-label col-md-3">Last Name</label>
              <div class="col-md-9">
                <input name="lastName" placeholder="Last Name" class="form-control" type="text">
              </div>
            </div> -->
<!--
            <div class="form-group">
              <label class="control-label col-md-3">Jenis Inventaris</label>
              <div class="col-md-9">
                <label class="control-label col-md-3">Jenis Inventaris</label>
              </div>
            </div>
              <div class="form-group">
               <button class="btn btn-info pull-right" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i> Tambah Jenis</button>
              </div>
-->

<!--
            <div class="form-group">
              <label class="control-label col-md-3">Jumlah</label>
              <div class="col-md-9">
                <input name="jumlah" placeholder="Jumlah" class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Keadaan</label>
              <div class="col-md-9">
                <select name="keadaan" class="form-control">
                  <option value="baik">Baik</option>
                  <option value="rusak">Rusak</option>
                  <option value="kurang">Kurang</option>
                </select>
              </div>
            </div>
-->

           <!--  <div class="form-group">
              <label class="control-label col-md-3">Date of Birth</label>
              <div class="col-md-9">
                <!-- <input name="dob" placeholder="yyyy-mm-dd" class="form-control" type="text"> -->
                                  <!-- Date dd/mm/yyyy -->
                    <!-- <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="dob" class="form-control" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask>
                    </div><!-- /.input group -->

             <!--  </div>
            </div> -->
          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
