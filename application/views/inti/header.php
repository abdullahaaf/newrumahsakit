<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CRUD_CI | ALIM DEV</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap.min.css');?>">
    <!-- Font Awesome Icons -->
    <link href="<?=base_url('assets/plugins/fontawesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="<?=base_url('assets/plugins/ionicons/css/ionicons.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- DATA TABLES -->
    <link href="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.css');?>" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/AdminLTE.min.css');?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/skins/_all-skins.min.css');?>">
     <!-- jQuery 2.1.4 -->
    <script src="<?=base_url('assets/plugins/jQuery/jQuery-2.1.4.min.js');?>"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>
    <script src="<?=base_url('assets/plugins/datatables/jquery.dataTables.min.js')?>"></script>
    <script src="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.js')?>"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
  <!-- the fixed layout is not compatible with sidebar-mini -->
  <body class="hold-transition skin-blue fixed sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="../../index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>S</b>iskol</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>SISKOL</b>_Media</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success">4</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 4 messages</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li><!-- start message -->
                        <a href="#">
                          <div class="pull-left">
                            <img src="<?=base_url('assets/dist/img/user2-160x160.jpg');?>" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Support Team
                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li><!-- end message -->
                    </ul>
                  </li>
                  <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">10</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 10 notifications</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">View all</a></li>
                </ul>
              </li>
              <!-- Tasks: style can be found in dropdown.less -->
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-flag-o"></i>
                  <span class="label label-danger">9</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 9 tasks</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Design some buttons
                            <small class="pull-right">20%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">20% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                    </ul>
                  </li>
                  <li class="footer">
                    <a href="#">View all tasks</a>
                  </li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?=base_url('assets/dist/img/user2-160x160.jpg');?>" class="user-image" alt="User Image">
                  <span class="hidden-xs">Alexander Pierce</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?=base_url('assets/dist/img/user2-160x160.jpg');?>" class="img-circle" alt="User Image">
                    <p>
                      Alexander Pierce - Web Developer
                      <small>Member since Nov. 2012</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="#" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url('assets/dist/img/user2-160x160.jpg');?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>Alexander Pierce</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="#">
                <i class="glyphicon glyphicon-education"></i> 
                <span>Akademik</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="../../index.html"><i class="fa fa-circle-o text-red"></i> Master</a></li>
                  <li><a href ='<?php echo site_url('akademik/penilaian/con_master_jenisujian')?>'><i class="fa fa-circle-o">Master Jenis Ujian</i></a></li>
                <li><a href="../../index2.html"><i class="fa fa-circle-o text-green"></i> Setting</a></li>
                <li><a href="../../index2.html"><i class="fa fa-circle-o text-aqua"></i> Penilaian</a></li>
              </ul>
            </li>
            <li class="treeview"> <!-- jika auto drobdown saat load + "active"-->
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Kesiswaan</span><i class="fa fa-angle-left pull-right"></i>
                <!-- <span class="label label-primary pull-right">4</span> -->
              </a>
              <ul class="treeview-menu">
                <li><a href="../layout/top-nav.html"><i class="fa fa-circle-o text-red"></i> Master</a></li>
                <li><a href="../layout/boxed.html"><i class="fa fa-circle-o text-yellow"></i> Report</a></li>
              </ul>
            </li>
            <li>
              <a href="../widgets.html">
                <i class="fa fa-retweet"></i> 
                <span>Sarana & Prasarana</span> <i class="fa fa-angle-left pull-right"></i>
                <!-- <small class="label pull-right bg-green">new</small> -->
              </a>
              <ul class="treeview-menu">
                <li><a href='<?php echo site_url('sarpras/con_master_sarpras')?>'><i class="fa fa-circle-o"></i> Master Inventaris</a></li>
                <li><a href="../layout/boxed.html"><i class="fa fa-circle-o text-yellow"></i> Report</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-comments"></i>
                <span>Humas</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href='<?php echo site_url('humas/con_master_humas')?>'><i class="fa fa-circle-o"></i> Master Jadwal Kegiatan</a></li>
                  <li><a href='<?php echo site_url('humas/suratmasuk/con_master_humas_suratmasuk')?>'><i class="fa fa-circle-o"></i> Master Surat Masuk</a></li>
                  <li><a href='<?php echo site_url('humas/suratkeluar/con_master_humas_suratkeluar')?>'><i class="fa fa-circle-o"></i> Master Surat Keluar</a></li>
                <li><a href="../charts/morris.html"><i class="fa fa-circle-o"></i> Report</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-child"></i>
                <span>BK</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li><a href='<?php echo site_url('bk/prestasi/con_master_bk_prestasi')?>'><i class="fa fa-circle-o"></i> Master Prestasi</a></li>
                  <li><a href='<?php echo site_url('bk/absensisiswa/con_master_bk_absensisiswa')?>'><i class="fa fa-circle-o"></i> Master Absensi Siswa</a></li>
                  <li><a href='<?php echo site_url('bk/absensipegawai/con_master_bk_absensipegawai')?>'><i class="fa fa-circle-o"></i> Master Absensi Pegawai</a></li>
                <li><a href="../UI/buttons.html"><i class="fa fa-circle-o"></i> Report</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="glyphicon glyphicon-book"></i> <span>Perpustakaan</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="../forms/general.html"><i class="fa fa-circle-o"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                      <li><a href='<?php echo site_url('perpus/con_master_buku')?>'><i class="fa fa-circle-o"></i> Master Buku</a></li>
                      <li><a href='<?php echo site_url('perpus/con_master_rak')?>'><i class="fa fa-circle-o"></i> Master Rak</a></li>
                      <li><a href='<?php echo site_url('perpus/con_master_pinjam_dan_denda')?>'><i class="fa fa-circle-o"></i> Master Pinjam & Denda</a></li>
                    </ul>
                </li>
                <li><a href="../forms/advanced.html"><i class="fa fa-circle-o"></i> Report</a></li>
                <li><a href="../forms/editors.html"><i class="fa fa-circle-o"></i> Operasi</a></li>
                <li><a href="../forms/general.html"><i class="fa fa-circle-o"></i> Grafik</a></li>
                <li><a href="../forms/advanced.html"><i class="fa fa-circle-o"></i> Catatan</a></li>
                <li><a href="../forms/editors.html"><i class="fa fa-circle-o"></i> Pengaturan</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="glyphicon glyphicon-usd"></i> <span>Keuangan</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
               

                <li><a href="../tables/data.html"><i class="fa fa-circle-o"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                    <li><a href='<?php echo site_url('keuangan/con_master_tahun_pembukuan')?>'><i class="fa fa-circle-o"></i> Tahun Pembukuan</a></li>

                     <li><a href='<?php echo site_url('keuangan/con_master_jenis_pembukuan')?>'><i class="fa fa-circle-o"></i> Jenis Pembukuan</a></li>
                     <li><a href='<?php echo site_url('keuangan/con_master_uraian_pembukuan')?>'><i class="fa fa-circle-o"></i> Uraian Pembukuan</a></li>
                     <li><a href='<?php echo site_url('keuangan/con_master_jenis_pembayaran')?>'><i class="fa fa-circle-o"></i> Jenis Pembayaran</a></li>
                     <li><a href='<?php echo site_url('keuangan/con_master_uraian_pembayaran')?>'><i class="fa fa-circle-o"></i> Uraian Pembayaran</a></li>
                     <li><a href='<?php echo site_url('keuangan/con_master_rincian_gaji_pegawai')?>'><i class="fa fa-circle-o"></i> Rincian Gaji Pegawai</a></li>

                     </ul>
                </li>

                <li><a href="../tables/data.html"><i class="fa fa-circle-o"></i> Transaksi</a></li>


                <li><a href="../tables/data.html"><i class="fa fa-circle-o"></i> Keuangan Sekolah<i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                    <li><a href='<?php echo site_url('keuangan/con_keuangan_sekolah_kas_masuk')?>'><i class="fa fa-circle-o"></i> Kas Masuk</a></li>
                    <li><a href='<?php echo site_url('keuangan/con_transaksi_pembayaran')?>'><i class="fa fa-circle-o"></i>Transaksi</a></li>
                    </ul>
                </li>

                  

              </ul>
            </li>
            <li>
              <a href="../calendar.html">
                <i class="fa fa-envelope"></i> <span>Sms Gateway</span>
                <small class="label pull-right bg-red">3</small>
              </a>
            </li>            
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>


      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            .: Sifokol V.1 :.
            <small>(Sistem Informasi Sekolah)</small>
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">