<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Master_Sarpras_Manajemen_Barang extends CI_Model{

	var $table = 'manajemenBarang,ruang,barang';
	var $column = array('id_mb','','nama_r','kelas_r','nama_i','jumlahBarang');
	var $order = array('id_mb' => 'desc');

  public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function tampil_data_ruang()
	{
        $this->db->select('nama_r,kelas_r');
        $this->db->from('ruang');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
	}

	function tampil_data_barang()
	{
        $this->db->select('nama_i');
        $this->db->from('barang');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
	}

	private function _get_datatables_query()
	{
		$this->db->from($this->table);
		$i = 0;

		foreach ($this->column as $item)
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}

		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id_mb)
	{
		$this->db->from($this->table);
		$this->db->where('id_mb',$id_mb);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table,$data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_mb)
	{
		$this->db->where('id_mb', $id_mb);
		$this->db->delete($this->table);
	}
}
