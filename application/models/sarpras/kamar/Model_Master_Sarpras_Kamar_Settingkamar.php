<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Master_Sarpras_Kamar_Settingkamar extends CI_Model{

	var $table = 'data_setting_kamar,data_kamar,data_ruangan,data_kelas,data_gedung';
	var $column = array('data_kamar.namaKamar','data_ruangan.namaRuangan','data_kelas.namaKelas','data_gedung.namaGedung');
	var $order = array('idSetKamar' => 'desc');
  var $relasi = 'data_setting_kamar.idKamar = data_kamar.idKamar AND data_setting_kamar.idRuangan = data_ruangan.idRuangan AND data_setting_kamar.idKelas = data_kelas.idKelas AND data_setting_kamar.idGedung = data_gedung.idGedung';

  public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function tampil_data_kamar()
	{
        $this->db->select('idKamar,namaKamar');
        $this->db->from('data_kamar');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
	}

	function tampil_data_kelas()
	{
        $this->db->select('idKelas,namaKelas');
        $this->db->from('data_kelas');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
	}

    function tampil_data_ruang()
	{
		$this->db->select('idRuangan,namaRuangan');
        $this->db->from('data_ruangan');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
	}

    function tampil_data_gedung()
	{
		$this->db->select('idGedung,namaGedung');
        $this->db->from('data_gedung');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
	}

	private function _get_datatables_query()
	{
		$this->db->from($this->table);
		// untuk relasi tambahkan ini
		$this->db->where($this->relasi);

		$i = 0;

		foreach ($this->column as $item)
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}

		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($idSetKamar)
	{
		$this->db->from('data_setting_kamar');
		$this->db->where('idSetKamar',$idSetKamar);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert('data_setting_kamar', $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update('data_setting_kamar', $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($idSetKamar)
	{
		$this->db->where('idSetKamar', $idSetKamar);
		$this->db->delete('data_setting_kamar');
	}


}
